<?php
/**
 * Created by IntelliJ IDEA.
 * User: byllmcsony
 * Date: 28/11/2018
 * Time: 12:53
 */

namespace App\Loader;
use App\Manager\CacheManager;


/**
 * Class CacheLoader
 * @package App\Loader
 */
class CacheLoader extends BaseLoader
{
    protected $manager;
    /**
     * CacheLoader constructor.
     */
    public function __construct()
    {
        parent::__construct(new CacheManager(), new DataBaseLoader());
    }

}
