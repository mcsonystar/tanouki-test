<?php
/**
 * Created by IntelliJ IDEA.
 * User: byllmcsony
 * Date: 28/11/2018
 * Time: 12:53
 */

namespace App\Loader;


use App\Manager\ApiManager;

/**
 * Class ApiLoader
 * @package App\Loader
 */
class ApiLoader extends BaseLoader
{
    /**
     * ApiLoader constructor.
     */
    public function __construct()
    {
        parent::__construct(new ApiManager());
    }
}
