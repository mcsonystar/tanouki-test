<?php
/**
 * Created by IntelliJ IDEA.
 * User: byllmcsony
 * Date: 28/11/2018
 * Time: 12:50
 */

namespace App\Loader;


use App\Manager\DataBaseManager;

class DataBaseLoader extends BaseLoader
{
    protected $manager;
    /**
     * DataBaseLoader constructor.
     */
    public function __construct()
    {
        parent::__construct(new DataBaseManager(), new ApiLoader());
    }
}
