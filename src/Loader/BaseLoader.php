<?php
/**
 * Created by IntelliJ IDEA.
 * User: byllmcsony
 * Date: 28/11/2018
 * Time: 12:48
 */
namespace App\Loader;
use App\Exception\NotFoundException;
use App\Manager\BaseManager;

/**
 * Class Loader
 * @package App\Loader
 */
abstract class BaseLoader
{
    /**
     * @var BaseLoader $delegate
     */
    protected $delegate = null;
    /**
     * @var BaseManager $manager;
     */
    protected $manager;

    /**
     * BaseLoader constructor.
     * @param BaseManager $manager
     * @param BaseLoader|null $delegate
     */
    public function __construct(BaseManager $manager, BaseLoader $delegate = null)
    {
        $this->delegate = $delegate;
        $this->manager = $manager;
    }


    /**
     * @param array $params
     * @return \App\Entity\ExchangeRate[]|array
     */
    public function processing(array $params) {
        $result = false;
        try {
            $result = $this->manager->getOr404($params);
        } catch (NotFoundException $e) {
            $result = $this->delegate->processing($params);
        } catch (\Exception $e) {
            /**
             * @todo do something
             */
        }
        finally {
            if (!$result) {
                /**
                 * @todo что делать если данные не найденые после проверки всех источников
                 */
            }
        }
        return $result;
    }
}
