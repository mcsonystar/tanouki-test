<?php
/**
 * Created by IntelliJ IDEA.
 * User: byllmcsony
 * Date: 28/11/2018
 * Time: 13:48
 */

namespace App\Manager;

abstract class BaseManager implements ManagerInterface
{
    /**
     * @var BaseManager[]
     */
    protected $managers = [];


    /**
     * BaseLoader constructor.
     * @param array $additionalManagers
     */
    public function __construct(array $additionalManagers = [])
    {
        $this->managers = $additionalManagers;
    }

    /**
     * @param array $params
     * @throws \App\Exception\NotFoundException
     * @throws \Exception
     */
    public function getOr404(array $params)
    {
        return $this->proceedSave($this->findOr404($params));
    }

    /**
     * @param array $data
     * @return bool
     * @throws \Exception
     */
    public function proceedSave($data)
    {
        $this->save($data);

        foreach ($this->managers as $manager) {
            try {
                $manager->save($data);
            } catch (\Exception $e) {
                /**
                 * @todo что делать если не смогли сохранить
                 */
            }
        }

        return true;
    }
}
