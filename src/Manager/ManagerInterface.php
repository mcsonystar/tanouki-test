<?php
/**
 * Created by IntelliJ IDEA.
 * User: byllmcsony
 * Date: 28/11/2018
 * Time: 13:49
 */
namespace App\Manager;

use App\Exception\NotFoundException;

interface ManagerInterface
{
    /**
     * @param array $params
     * @return \App\Entity\ExchangeRate[]
     * @throws NotFoundException
     */
    public function findOr404(array $params);

    /**
     * @param array $data
     * @return boolean
     * @throws \Exception
     */
    public function save($data);
}
