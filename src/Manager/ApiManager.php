<?php
/**
 * Created by IntelliJ IDEA.
 * User: byllmcsony
 * Date: 28/11/2018
 * Time: 13:45
 */

namespace App\Manager;


use App\Exception\NotFoundException;

class ApiManager extends BaseManager
{
    /**
     * ApiLoader constructor.
     */
    public function __construct()
    {
        parent::__construct([new CacheManager(), new DataBaseManager()]);
    }

    /**
     * @param array $params
     * @return \App\Entity\ExchangeRate[]
     * @throws NotFoundException
     */
    public function findOr404(array $params)
    {
        return ['new'];
    }

    /**
     * @param array $data
     *
     * @return bool
     * @throws \Exception
     */
    public function save($data)
    {
        /**
         * @todo что делать если не смогли сохранить?
         */
        return true;
    }
}
